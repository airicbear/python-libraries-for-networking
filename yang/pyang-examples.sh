echo "Print the YANG module in a simple text tree"
pyang -f tree ietf-interfaces.yang

echo "Print only part of the tree"
pyang -f tree --tree-path=/interfaces/interface ietf-interfaces.yang

echo "Print an example XML skeleton (NETCONF)"
pyang -f sample-xml-skeleton ietf-interfaces.yang

echo "Create an HTTP/JS view of the YANG Model"
if [ -f "ietf-interfaces.html" ] ; then
  rm "ietf-interfaces.html"
fi
pyang -f jstree -o ietf-interfaces.html ietf-interfaces.yang
start ietf-interfaces.html

echo 'Control the "nested depth" in trees'
pyang -f tree --tree-depth=2 ietf-ip.yang

echo "Include deviation models in the processing"
pyang -f tree --deviation-module=cisco-xe-ietf-ip-deviation.yang ietf-ip.yang